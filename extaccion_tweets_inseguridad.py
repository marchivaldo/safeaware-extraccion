import nltk
import os
import pandas as pd
import tweepy
import re
import string
from textblob import TextBlob
import preprocessor as p
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

nltk.download('stopwords')
nltk.download('punkt')

#Los permisos de la cuenta de desarrollador Twitter
consumer_key = 'xxxxx'
consumer_secret = 'xxxxx'
access_key= 'xxxxx'
access_secret = 'xxxxxxx'

#Pasar los permisos a la api para autenticación
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_key, access_secret)
api = tweepy.API(auth)

#Definición de la ruta para guardar el archivo .csv
incidents_tweets = "data/insecurity_data_extraction/insecurity_data.csv"

#Las columnas para el archivo csv
COLS = ['id', 'created_at', 'source', 'original_text','clean_text', 'sentiment','polarity','subjectivity', 'lang',
        'favorite_count', 'retweet_count', 'original_author', 'possibly_sensitive', 'hashtags',
        'user_mentions', 'place', 'place_coord_boundaries']

#Definimos dos variables que definirán el periodo de tiempo del que se extraerán los tweets
start_date = '2018-10-01'
end_date = '2018-10-31'

# Happy emoticons
emoticons_happy = set([
    ':-)', ':)', ';)', ':o)', ':]', ':3', ':c)', ':>', '=]', '8)', '=)', ':}',
    ':^)', ':-D', ':D', '8-D', '8D', 'x-D', 'xD', 'X-D', 'XD', '=-D', '=D',
    '=-3', '=3', ':-))', ":'-)", ":')", ':*', ':^*', '>:P', ':-P', ':P', 'X-P',
    'x-p', 'xp', 'XP', ':-p', ':p', '=p', ':-b', ':b', '>:)', '>;)', '>:-)',
    '<3'
    ])

# Sad emoticons
emoticons_sad = set([
    ':L', ':-/', '>:/', ':S', '>:[', ':@', ':-(', ':[', ':-||', '=L', ':<',
    ':-[', ':-<', '=\\', '=/', '>:(', ':(', '>.<', ":'-(", ":'(", ':\\', ':-c',
    ':c', ':{', '>:\\', ';('
    ])

#Patrones de emoji
emoji_pattern = re.compile("["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           u"\U00002702-\U000027B0"
                           u"\U000024C2-\U0001F251"
                           "]+", flags=re.UNICODE)

#combinación de sad y happy emoticons
emoticons = emoticons_happy.union(emoticons_sad)


#mrhod clean_tweets()
def clean_tweets(tweet):
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(tweet)

    #Después de el preprocessing de tweepy eliminamos la columna izquierda con menciones
    #o retweets al inicio
    tweet = re.sub(r':', '', tweet)
    tweet = re.sub(r'‚Ä¶', '', tweet)
    #remplazo de caracteres distintos a ASCII por un espacio
    tweet = re.sub(r'[^\x00-\x7F]+',' ', tweet)


    #eliminamos emojis del tweet
    tweet = emoji_pattern.sub(r'', tweet)

    #filtro usando la librería NLTK y se apendiza a una cadena
    filtered_tweet = [w for w in word_tokens if not w in stop_words]
    filtered_tweet = []

    #Ciclo a través de las condiciones
    for w in word_tokens:
        if w not in stop_words and w not in emoticons and w not in string.punctuation:
            filtered_tweet.append(w)
    return ' '.join(filtered_tweet)
    #print(word_tokens)
    #print(filtered_sentence)

#method write_tweets()
def write_tweets(keyword, file):
    #Si el archivo existe, leer los datos del archivo csv
    if os.path.exists(file):
        df = pd.read_csv(file, header=0)
    else:
        df = pd.DataFrame(columns=COLS)
    #atributo de página tweepy.cursor e iteración
    for page in tweepy.Cursor(api.search, q=keyword,
                              count=200, include_rts=False, since=start_date).pages(50):
        for status in page:
            new_entry = []
            status = status._json

            ## Checa si el tweet está en inglés, si no, pasa a la siguiente instrucción
            if status['lang'] != 'en':
                continue

            #Este código remplaza la cantidad de retweets
            #y el número de favoritos desde la última descarga
            if status['created_at'] in df['created_at'].values:
                i = df.loc[df['created_at'] == status['created_at']].index[0]
                if status['favorite_count'] != df.at[i, 'favorite_count'] or \
                   status['retweet_count'] != df.at[i, 'retweet_count']:
                    df.at[i, 'favorite_count'] = status['favorite_count']
                    df.at[i, 'retweet_count'] = status['retweet_count']
                continue


           #tweepy preprocessing para realizar preprocesado básico
            clean_text = p.clean(status['text'])

            #llamada a clean_tweet para preprocessing extra
            filtered_tweet=clean_tweets(clean_text)

            #Método textBlob para cálculo de sentimientos
            blob = TextBlob(filtered_tweet)
            Sentiment = blob.sentiment

            #separar polaridad y subjetividad en dos variables
            polarity = Sentiment.polarity
            subjectivity = Sentiment.subjectivity

            #nuevo indice de entrada
            new_entry += [status['id'], status['created_at'],
                          status['source'], status['text'],filtered_tweet, Sentiment,polarity,subjectivity, status['lang'],
                          status['favorite_count'], status['retweet_count']]

            #indexar el nuevo autor del tweet
            new_entry.append(status['user']['screen_name'])

            try:
                is_sensitive = status['possibly_sensitive']
            except KeyError:
                is_sensitive = None
            new_entry.append(is_sensitive)

            # hashtagas y menciones son almacenadas usando comas
            hashtags = ", ".join([hashtag_item['text'] for hashtag_item in status['entities']['hashtags']])
            new_entry.append(hashtags)
            mentions = ", ".join([mention['screen_name'] for mention in status['entities']['user_mentions']])
            new_entry.append(mentions)

            #Se obtiene la ubicación del tweet si es posible
            try:
                location = status['user']['location']
            except TypeError:
                location = ''
            new_entry.append(location)

            try:
                coordinates = [coord for loc in status['place']['bounding_box']['coordinates'] for coord in loc]
            except TypeError:
                coordinates = None
            new_entry.append(coordinates)

            single_tweet_df = pd.DataFrame([new_entry], columns=COLS)
            df = df.append(single_tweet_df, ignore_index=True)
            csvFile = open(file, 'a' ,encoding='utf-8')
    df.to_csv(csvFile, mode='a', columns=COLS, index=False, encoding="utf-8")

#declaración de las palabras clave para la busqueda por categorías
incidents_keywords = '#murder OR #violence OR #homicide OR #insecurity'

#lamada al método main pasando como parámetro las palabras clave y la ruta de guardado del archivo csv
write_tweets(incidents_keywords,  incidents_tweets)
